# Datamodels

This repository describes how device data will be represented in Orion Context Broker using the OMA NGSI specification.
The models presented here are adaptations created based on
the [FIWARE Smart Data Models](https://github.com/smart-data-models), extended with additional data provided by our
SmartSpot devices and complemented with metadata to aid data readability and validation.

## Get Started

- [Libelium FIWARE SaaS Data Models](docs/datamodels.md)
- [Contributing](docs/contributing.md)
- [References](docs/references.md)
